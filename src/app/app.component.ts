import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {UserService} from "./services/user/user.service";
import {User} from "./models/user.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  users: Array<User>;

  usersSubscription: Subscription;

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.usersSubscription = this.userService.getUsers().subscribe(
      (users: Array<User>) => this.users = users
    );
  }

  ngOnDestroy() {
    this.usersSubscription.unsubscribe();
  }
}
