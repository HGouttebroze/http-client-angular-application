import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject} from "rxjs";
import {User} from "../../models/user.model";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class UserService {

  apiUrl = 'https://reqres.in';

  users: BehaviorSubject<Array<User>>;

  constructor(private httpClient: HttpClient) {
    this.users = new BehaviorSubject<Array<User>>([]);
  }

  /**
   * Methode for retrieve all the users from the API
   */
  getUsers() {

    return this.httpClient
      .get(this.apiUrl + '/api/users?page=2')
      .pipe(
        map((res:any) => {
          const arrayUsers = res.data.map(item => {
            return new User(item.id, item.email, item.first_name, item.last_name, item.avatar);
          });

          this.users.next(arrayUsers);
          return this.users.getValue();
        })
      );

  }
}
